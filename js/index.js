"use strict";

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). 
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const generalContainer = document.createElement("div");
generalContainer.setAttribute("id", "root");
console.log(generalContainer);

class CatchBookError extends Error {
    constructor(book, description) {
        

        super(`Wrong Book: Missed ${description}`);
        this.name = "CatchBookError";
        // this.description = description;
        if (book.name === undefined) {

        };

        if (book.price === undefined) {

        };

        if (book.author === undefined) {

        };
    };
};

class BookError {
    constructor(book) {
        if (!("price" in book)) {
            throw new CatchBookError(book, "price");
            
            // if (book.name === "") {
                
            // };
        };

        if (!("author" in book)) {
            throw new CatchBookError(book, "author");
            // if (book.name === "") {
                
            // };
        };

        if (!("name" in book)) {
            throw new CatchBookError(book, "name");
            // if (book.name === "") {
                
            // };
        };
        // || (!("author" in book)) || (!("name" in book)
        this.book = book;
    };
}

class BookRender {
    constructor(book) {
        if ("price" in book && "author" in book && "name" in book) {

        }

        this.book = book;
    };

    render(generalContainer) {
        generalContainer.insertAdjacentHTML("beforeend", `
            <ul class="item">
            <li>${this.book.name}, ${this.book.author}, ${this.book.price}</li>
            </ul>
        `);
        document.body.append(generalContainer);
    };
}


books.forEach(elem => {
    try {
        new BookError(elem);
        new BookRender(elem).render(generalContainer);

    } catch (err) {
        if (err.name === "CatchBookError") {
            console.warn(err);
        } else {
            throw err;
        }
    }
});




/* "use strict";

const colors = [
    '#92a8d1',
    '#deeaee',
    '#b1cbbb',
    '#c94c4c',
    '#f7cac9',
    '#f7786b',
    '#82gdfgb74b',
    '#405d27',
    '#405d271231',
    '#b5e7a0',
    '#eca1a6',
    '#d64161',
    '#b2ad7f',
    '#6b5b95',
];

const generalContainer = document.querySelector(".colors-container");

class HexColorError extends Error {
    constructor(color) {
        super(`Wrong HEX color: ${color}`);
        this.name = "HexColorError";
    }
}

class ColorBlock {
    constructor(color) {
        if (color.length > 7) {
            throw new HexColorError(color);   
        } 

        this.color = color;
    };

    render(container) {
        container.insertAdjacentHTML("beforeend", `
            <div class="item" style="background-color: ${this.color}">
            <span>${this.color}</span>
            </div>
        `);
    };
}

colors.forEach(elem => {
    try {
        // if (elem.length > 7) {
        //     throw new HexColorError(elem);   
        // } 

        new ColorBlock(elem).render(generalContainer);

        // if (elem.length <= 7) {
        //     new ColorBlock(elem).render(generalContainer);
        // } else {
        //     throw new HexColorError(elem);   
        // }

    } catch (err) {
        if(err.name === 'HexColorError') {
            console.warn(err);
        } else {
            throw err;
        }
    };
  
}); */




/*
Дан масив кольорів у форматі HEX

Виведіть на екран палітру, що складається з:

<div class="item" >
     <span>#000000</span>
</div>

де #000000 це код кольору. Задати div бекграунд з цим кольором

Якщо код кольору не відповідає формату HEX – 
виведе в консоль помилку (в пмилці має бути зазначений неправильний код) та не малюйте блок у палітру;

P.S. Використовуйте try...catch, 
свій тип помилки успадкований від Error, всі інші помилки прокиньте далі.
*/
